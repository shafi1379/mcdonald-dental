module.exports = {
  mode: "jit",
  purge: ["./src/**/**/*.{ts,tsx}"],
  darkMode: true, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        grey: {
          light: "#C3CACE",
          medium: "#757B7E",
          thin: "rgba(73, 98, 144, 0.25)",
        },
        blue: {
          dark: "#37548C",
        },
        orange: {
          medium: "#D26048",
          mcd: "#D25E44",
        },
        balck: {
          meduim: "#273135",
        },
        white: {
          off: "#F4F2EC",
          DEFAULT: "#FFFFFF",
        },
        hash: {
          ligth: "#F4F2EC",
        },
      },
      maxWidth: {
        300: "300px",
        367: "367px",
        400: "400px",
        450: "450px",
        600: "600px",
      },
      padding: {
        15: "3.75rem",
      },
    },
    container: {
      center: true,
      padding: {
        padding: "3rem",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
