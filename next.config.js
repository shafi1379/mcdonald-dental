module.exports = {
  sass: true,
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  images: {
    domains: ["https://images.prismic.io/"],
    hostname: "images.prismic.io",
  },
};
