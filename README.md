# mcDonald-Dental

### Run the development server:

```bash
npm install
# once installatio complete, run below command.
npm run dev
```

### Run the build for production as below:

```bash
npm run build
# after build creation, run below. 
npm start/ npm run start
```

```bash
docker build . -t mcdonald-dental
# then exceute below command.
docker run -p 3000:3000 mcdonald-dental
```

### Lint your code before commit:

```bash
npm run lint-staged
# Or
npm i -g pretty-quick | pretty-quick
```