import type { AppProps } from "next/app";
import "../styles/_styles.scss";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div className="main-content">
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
