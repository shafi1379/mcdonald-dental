import { GetServerSideProps, NextPage } from "next";
import Image from "next/image";
import ImgDesc from "../components/imgDesc";
import ImgBanner from "../components/imgBanner";
import Enquiry from "../components/enquiry";
import { SliceType, TextDescription } from "../utilities/util";
import Prismic from "@prismicio/client";
import { Client } from "../utilities/prismicHelpers";
import React, { ReactNode } from "react";
import Features from "../components/features";
import Header from "../components/header";
import Footer from "../components/footer";

type Props = {
  children?: ReactNode;
  home: any;
};

const Home: NextPage = ({ home }: Props) => {
  const HeaderSection = home?.body?.filter(
    (item) => item.slice_type === SliceType.HeaderSection
  )[0].primary;
  const FeaturesSection = home?.body?.filter(
    (item) => item.slice_type === SliceType.FeaturesSection
  );
  const ImageBannerSection = home?.body?.filter(
    (item) => item.slice_type === SliceType.ImageBannerSection
  );
  const ImageDescSection = home?.body?.filter(
    (item) => item.slice_type === SliceType.ImageDescSection
  );
  const EnquirySection = home?.body?.filter(
    (item) => item.slice_type === SliceType.EnquirySection
  );
  const FooterSection = home?.body?.filter(
    (item) => item.slice_type === SliceType.FooterSection
  );
  return (
    <main className="mx-auto">
      <Header>
        <Image
          layout="fill"
          objectFit="contain"
          src={`/api/imageproxy?url=${encodeURIComponent(
            HeaderSection.background_image.url
          )}`}
          alt={HeaderSection.background_image.alt}
        />
        <div className="bg-cover landing-page"></div>
        <div className="content">
          <div className="text-center mx-auto">
            {HeaderSection.title.map((title: TextDescription, ind: number) => (
              <h1 key={ind} className="text-[42px] font font-normal text-white">
                {title.text}
                {HeaderSection.title.length === ind + 1 && (
                  <span className="text-[#d25f44]">.</span>
                )}
              </h1>
            ))}
            {HeaderSection.paragraph.map(
              (para: TextDescription, ind: number) => (
                <p
                  key={ind}
                  className="text-[21px] mt-4 text-grey-light font-thin"
                >
                  {para.text}
                </p>
              )
            )}
            <p className="mt-12">
              <button
                className="text-white bg-orange-mcd pl-5 pr-5 py-2 focus:ring-4 rounded-3xl"
                aria-label="Appointment"
              >
                {HeaderSection.button_label}
                <i
                  className="fa fa-long-arrow-right h-6 w-7 pl-2"
                  aria-hidden="true"
                ></i>
              </button>
            </p>
          </div>
        </div>
      </Header>
      <Features categories={FeaturesSection[0]} />
      <ImgBanner banner={ImageBannerSection[0].primary} />
      <ImgDesc details={ImageDescSection[0].primary} />
      <ImgBanner banner={ImageBannerSection[1].primary} />
      <ImgDesc details={ImageDescSection[1].primary} />
      <ImgBanner banner={ImageBannerSection[2].primary} />
      <ImgDesc details={ImageDescSection[2].primary} />
      <Enquiry details={EnquirySection[0].primary} />
      <Footer {...FooterSection[0]} />
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const home = await Client().query(
    Prismic.Predicates.at("document.type", "home")
  );

  return {
    props: {
      home: home.results[0].data,
    },
  };
};

export default Home;
