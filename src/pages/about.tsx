import { GetServerSideProps, NextPage } from "next";
import Image from "next/image";
import React, { ReactNode } from "react";
import ImgDesc from "../components/imgDesc";
import Prismic from "@prismicio/client";
import { Client } from "../utilities/prismicHelpers";
import { SliceType, TextDescription } from "../utilities/util";
import AboutProfile from "../components/aboutProfile";
import Customers from "../components/customers";
import Header from "../components/header";
import Footer from "../components/footer";

type Props = {
  children?: ReactNode;
  about: any;
};

const About: NextPage = ({ about }: Props) => {
  const HeaderSection = about?.body?.filter(
    (item) => item.slice_type === SliceType.HeaderSection
  )[0].primary;
  const ProfileDetails = about?.body?.filter(
    (item) => item.slice_type === SliceType.AboutDescSection
  );
  const Sponsers = about?.body?.filter(
    (item) => item.slice_type === SliceType.CustomerLogos
  );
  const ImageDescSection = about?.body?.filter(
    (item) => item.slice_type === SliceType.ImageDescSection
  );

  const FooterSection = about?.body?.filter(
    (item) => item.slice_type === SliceType.FooterSection
  );

  return (
    <main>
      <Header>
        <Image
          layout="fill"
          objectFit="cover"
          src={`/api/imageproxy?url=${encodeURIComponent(
            HeaderSection.background_image.url
          )}`}
          alt={HeaderSection.background_image.alt}
        />
        <div className="bg-cover about-page"></div>
        <div className="content">
          <div className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-4">
            <div className="text-center mx-auto py-5 md:py-10">
              <Image
                className="rounded-lg"
                src={`/api/imageproxy?url=${encodeURIComponent(
                  HeaderSection.header_image.url
                )}`}
                width={331}
                height={251}
                alt={HeaderSection.header_image.alt}
              />
            </div>
            <div className="text-center md:text-left md:py-24 text-white">
              {HeaderSection.paragraph.map(
                (para: TextDescription, ind: number) => (
                  <h1
                    key={ind}
                    className="text-[16px] font font-normal text-grey-light"
                  >
                    {para.text}
                  </h1>
                )
              )}
              {HeaderSection.title1.map(
                (title: TextDescription, ind: number) => (
                  <h1
                    key={ind}
                    className="text-[42px] font font-normal leading-normal"
                  >
                    {title.text}
                  </h1>
                )
              )}
            </div>
          </div>
        </div>
      </Header>
      <AboutProfile details={ProfileDetails[0]} />
      <Customers sponsers={Sponsers[0]} />
      <ImgDesc details={ImageDescSection[0].primary} />
      <Footer {...FooterSection[0]} />
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const about = await Client().query(
    Prismic.Predicates.at("document.type", "about")
  );

  return {
    props: {
      about: about.results[0].data,
    },
  };
};

export default About;
