type KeyValuePair = {
  [key: string]: string;
};

export type RouteType = { route: string; title: string };

export type SocialMediaLink = {
  media_btn: string;
  media_link: string;
};

export type ImageDetail = {
  url: string;
  dimensions: {
    height: string;
    width: string;
  };
  alt: string;
};

export type TextDescription = {
  text: string;
  type: string;
};

export type ServiceCatagories = {
  icon: string;
  bgcolor: string;
  header: string;
  description: string;
};

export const PageRoute: KeyValuePair = {
  "/": "home",
  "/about": "about",
};

export const SliceType: KeyValuePair = {
  HeaderSection: "header_section",
  ImageBannerSection: "image_banner_section",
  ImageDescSection: "image_desc_section",
  EnquirySection: "enquiry_section",
  AboutDescSection: "about_desc_section",
  CustomerLogos: "customer_logos",
  FooterSection: "footer_section",
  FeaturesSection: "features_section",
};

export const AppRuoters: RouteType[] = [
  { title: "Services", route: "/" },
  { title: "About", route: "/about" },
  {
    title: "Make an appointment",
    route: "/appointment",
  },
];
