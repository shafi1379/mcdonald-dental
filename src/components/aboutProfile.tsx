import Image from "next/image";
import { ImageDetail, TextDescription } from "../utilities/util";

type Props = {
  details?: {
    primary: {
      description: TextDescription[];
    };
    items: {
      image: ImageDetail;
    }[];
  };
};

const AboutProfile: React.FC<Props> = ({ details }: Props) => {
  return (
    <div className="mb-10 text-center mx-auto grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-4 md:mx-32">
      <div className="py-12 px-14">
        {details.items.map((item: { image: ImageDetail }, ind: number) => (
          <div key={ind} className="text-center mx-auto py-2">
            <Image
              className="rounded-lg"
              src={`/api/imageproxy?url=${encodeURIComponent(item.image.url)}`}
              width={item.image.dimensions.width}
              height={item.image.dimensions.height}
              alt={item.image.alt}
            />
          </div>
        ))}
      </div>
      <div className="max-w-600 col-span-2 text-left px-10 md:pt-20 md:pr-32 text-grey-medium">
        {details.primary.description.map(
          (desc: TextDescription, ind: number) => (
            <p className="py-2" key={ind}>
              {desc.text}
            </p>
          )
        )}
        <p className="py-5">Dr. E. D. McDonald</p>
        <div className="text-left mx-auto py-1">
          <Image
            className="rounded-lg"
            src="/img_4.png"
            width={200}
            height={120}
            alt="mcDonal Signature"
          />
        </div>
      </div>
    </div>
  );
};

export default AboutProfile;
