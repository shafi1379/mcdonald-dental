import Image from "next/image";
import Link from "next/link";
import {
  ImageDetail,
  SocialMediaLink,
  TextDescription,
} from "../utilities/util";

type Props = {
  primary: {
    location: TextDescription[];
    services: TextDescription[];
    contact_email: string;
    contact_phone: string;
    copyright: string;
    hours: TextDescription[];
    icon_image: ImageDetail;
    icon_title: TextDescription[];
    about: TextDescription[];
  };
  items: SocialMediaLink[];
  slice_type: string;
};

const Footer: React.FC<Props> = (props: Props) => {
  const socialMedias = props.items;
  const {
    location,
    services,
    contact_email,
    contact_phone,
    copyright,
    hours,
    icon_image,
    icon_title,
    about,
  } = props.primary;

  const SocialMediaIcon = (datas: SocialMediaLink[]) =>
    datas.map((media: SocialMediaLink, ind: number) => (
      <a key={ind} href={media.media_link} rel="noreferrer" className="mr-5">
        <i
          className={`fab fa-${media.media_btn} text-[24px]`}
          aria-hidden="true"
        ></i>
      </a>
    ));

  const Services = (datas: TextDescription[]) => (
    <ul className="mt-2 text-grey-light">
      {datas.map((service: TextDescription, ind: number) => (
        <li key={ind}>{service.text}</li>
      ))}
    </ul>
  );

  return (
    <footer className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-4 px-5 py-10 md:px-10 md:py-20 text-white md:divide-x divide-gray-700 bg-[#102448]">
      <div className="md:px-10">
        <div className="flex items-center mb-10">
          <Image
            src={`/api/imageproxy?url=${encodeURIComponent(icon_image?.url)}`}
            width={35}
            height={35}
            priority
            alt={icon_image?.alt || "mcDonald Dental logo"}
          />
          <Link href="/">
            <a
              rel="noreferrer"
              className="text-[18px] antialiased font-normal ml-3 text-white"
            >
              {icon_title[0].text}
            </a>
          </Link>
        </div>
        <div className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-4 md:mt-10">
          <div className="flex-row">
            <div className="mb-3">
              {SocialMediaIcon(
                socialMedias?.slice(0, socialMedias?.length / 2)
              )}
            </div>
            <div>
              {SocialMediaIcon(
                socialMedias.slice(
                  socialMedias?.length / 2,
                  socialMedias?.length
                )
              )}
            </div>
          </div>
          <div>
            <h3 className="text-[16px] font-bold">Contact</h3>
            <ul className="text-[16px]">
              <li className="mt-2 cursor-pointer">
                <i
                  className="far fa-phone mr-2 text-orange-medium"
                  aria-hidden="true"
                ></i>
                <a
                  href={`tel:${contact_phone}`}
                  rel="noreferrer"
                  className="text-grey-light"
                >
                  {contact_phone}
                </a>
              </li>
              <li className="mt-2 cursor-pointer">
                <i
                  className="far fa-envelope mr-2 text-orange-medium"
                  aria-hidden="true"
                ></i>
                <a
                  href={`mailto:${contact_email}`}
                  rel="noreferrer"
                  className="text-grey-light"
                >
                  {contact_email}
                </a>
              </li>
            </ul>
          </div>
          <div className="md:mt-5">
            <h3 className="text-[16px] font-bold md:mb-3">Location</h3>
            <div className="text-grey-light md:mr-20">
              {location?.map((loc: TextDescription, ind: number) => (
                <p key={ind}>{loc.text}</p>
              ))}
            </div>
          </div>
          <div className="md:mt-5">
            <h3 className="text-[16px] font-bold md:mb-3">Hours</h3>
            <div className="text-grey-light md:mr-20">
              {hours?.map((hr: TextDescription, ind: number) => (
                <p key={ind}>{hr.text}</p>
              ))}
            </div>
          </div>
        </div>
        <div className="mt-10">
          <p className="text-[11px] text-grey-light">
            Copyright <span className="text-orange-medium">©</span> {copyright}
          </p>
        </div>
      </div>
      <div className="md:px-10">
        <div className="grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-4">
          <div>
            <h3 className="text-[16px] font-bold">Services</h3>
            {Services(services?.slice(0, services?.length / 2))}
          </div>
          <div className="md:mt-7">
            {Services(services?.slice(services?.length / 2, services?.length))}
          </div>
          <div>
            <h3 className="text-[16px] font-bold">About</h3>
            <ul className="mt-2 text-grey-light">
              {about?.map((abt: TextDescription, ind: number) => (
                <li key={ind}>{abt.text}</li>
              ))}
            </ul>
          </div>
        </div>
        <div>
          <p className="text-[14px] italic text-grey-light mt-7 md:mt-20">
            <i
              className="far fa-exclamation-triangle w-5 h-5 mr-2 text-orange-medium"
              aria-hidden="true"
            ></i>
            <br />
            If you are experiencing a medical emergency, <br /> please call 911
            immediately.
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
