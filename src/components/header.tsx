import { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import Head from "next/head";
import { useRouter } from "next/router";
import cn from "classnames";
import { useWindowSize } from "../utilities/hook";
import { AppRuoters, RouteType } from "../utilities/util";

type Props = {
  children?: React.ReactNode;
};

const Header: React.FC<Props> = (props: Props) => {
  const { pathname } = useRouter();
  const currentPage: string = pathname.replace(/\//, "");
  const [mobileMenuIsOpen, setMobileMenuIsOpen] = useState(false);
  const { width } = useWindowSize();

  const capitalize = ([first, ...rest]: any) => {
    return first.toUpperCase() + rest.join("").toLowerCase();
  };

  return (
    <>
      <Head>
        <title>{currentPage ? capitalize(currentPage) : "Home"}</title>
        <meta
          name="Description"
          content={
            currentPage
              ? capitalize(currentPage)
              : "Home" + " page for mcDonald dental."
          }
        />
        <link rel="shortcut icon" href="/static/favicon.ico" />
      </Head>
      <header
        style={{ height: currentPage !== "about" ? "856px" : "auto" }}
        className={cn(
          "header",
          currentPage !== "about" ? "pt-72 clip" : "pt-64"
        )}
      >
        <div className="header-menu">
          <div className="header-menu-list flex flex-wrap items-center justify-between mx-auto md:px-15">
            <div className="flex items-center px-5 py-5">
              <Image
                src="/logo.png"
                width={35}
                height={35}
                priority
                alt="mcDonald Dental logo"
              />
              <Link href="/">
                <a
                  rel="noreferrer"
                  className="text-[18px] antialiased font-normal ml-3 text-white"
                >
                  McDonald Dental
                </a>
              </Link>
            </div>
            <button
              className="flex mr-5 items-center px-3 py-2 text-white border border-white rounded-lg md:hidden"
              aria-label="Application List"
              onClick={() => setMobileMenuIsOpen(!mobileMenuIsOpen)}
            >
              {mobileMenuIsOpen ? (
                <i className="fas fa-times"></i>
              ) : (
                <svg
                  className="w-3 h-3 fill-current"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Menu</title>
                  <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                </svg>
              )}
            </button>
            <ul
              className={cn(
                "text-[16px] md:flex px-5 py-3 flex-col md:flex-row md:items-center md:justify-center w-full md:w-auto",
                mobileMenuIsOpen ? `block` : `hidden`,
                "bg-[#102448]"
              )}
            >
              {AppRuoters.map(({ route, title }: RouteType) => (
                <li
                  className={cn(
                    route === "/appointment" && width > 700
                      ? "border border-white-500 py-1 px-4 rounded-2xl"
                      : "",
                    "md:mt-0 md:ml-6"
                  )}
                  key={title}
                >
                  {route === "/appointment" ? (
                    <p
                      className={cn(
                        route === "/appointment" && width < 994 ? "p-3" : "",
                        "cursor-pointer"
                      )}
                    >
                      <a rel="noreferrer" className="block text-white">
                        {title}
                      </a>
                    </p>
                  ) : (
                    <Link href={route}>
                      <a rel="noreferrer" className="p-3 block text-white">
                        {title}
                      </a>
                    </Link>
                  )}
                </li>
              ))}
            </ul>
          </div>
        </div>
        {props.children}
      </header>
    </>
  );
};

export default Header;
