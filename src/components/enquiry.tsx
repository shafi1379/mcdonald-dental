import { TextDescription } from "../utilities/util";

type Props = {
  details?: {
    title: TextDescription[];
    description: TextDescription[];
    phone_number: TextDescription[];
    email: TextDescription[];
  };
};

const Enquiry: React.FC<Props> = ({ details }: Props) => {
  return (
    <div className="px-5 md:px-15 text-white py-5 md:py-10 bg-[#233B6A]">
      <div className="flex md:mx-20 py-10">
        <div className="md:w-5/12 sm:w-full">
          <h3 className="text-[21px]">
            {details.title[0].text}
            <span className="text-[#d25f44]">.</span>
          </h3>
          <p className="mt-4 max-w-300 text-[16px] text-grey-light font-normal mb-10 pr-15">
            {details.description[0].text}
          </p>
          <ul className="text-[16px]">
            <li className="mt-2 cursor-pointer">
              <i
                className="far fa-phone mr-2 text-orange-medium"
                aria-hidden="true"
              ></i>
              <a
                href={`tel:${details.phone_number[0].text}`}
                rel="noreferrer"
                className="text-grey-light"
              >
                {details.phone_number[0].text}
              </a>
            </li>
            <li className="mb-2 md:mt-2 cursor-pointer">
              <i
                className="far fa-envelope mr-2 text-orange-medium"
                aria-hidden="true"
              ></i>
              <a
                href={`mailto:${details.email[0].text}`}
                rel="noreferrer"
                className="text-grey-light"
              >
                {details.email[0].text}
              </a>
            </li>
          </ul>
        </div>
        <div className="md:w-7/12 sm:w-full">
          <div className="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-1">
            <input
              type="text"
              placeholder="Full Name"
              className="form-input px-4 py-2 md:mb-1 rounded border-grey-thin bg-grey-thin text-grey-light"
              aria-label="Full Name"
            />
            <input
              type="email"
              placeholder="Email"
              className="form-input px-4 py-2 mb-1 rounded border-grey-thin bg-grey-thin text-grey-light"
              aria-label="Email"
            />
          </div>
          <div className="text-grey-light">
            <select
              aria-label="Service List"
              className="px-4 py-2 rounded border-grey-thin mb-1 w-full bg-grey-thin text-grey-medium"
            >
              <option>Choose a service</option>
            </select>
          </div>
          <div>
            <textarea
              className="w-full mb-1 border-grey-thin bg-grey-thin rounded text-grey-light"
              rows={3}
              aria-label="Descriptions"
              role="textbox"
              placeholder="How can we help?"
            ></textarea>
          </div>
          <button
            className="float-right rounded-full text-grey-light border border-grey-thin py-1 px-5"
            aria-label="Submit"
          >
            Submit
            <i
              className="fa fa-long-arrow-right ml-2 text-orange-medium"
              aria-hidden="true"
            ></i>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Enquiry;
