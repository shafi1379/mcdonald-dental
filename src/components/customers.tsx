import Image from "next/image";
import { ImageDetail } from "../utilities/util";

type Props = {
  sponsers?: {
    items: {
      logo: ImageDetail;
    }[];
  };
};

const Customers: React.FC<Props> = ({ sponsers }: Props) => {
  return (
    <div className="bg-hash-ligth mx-auto grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-4 py-20 px-20">
      {sponsers.items.map((sponser, ind: number) => (
        <div key={ind} className="mx-auto">
          <Image
            src={`/api/imageproxy?url=${encodeURIComponent(sponser.logo.url)}`}
            width={sponser.logo.dimensions.width}
            height={sponser.logo.dimensions.height}
            alt={sponser.logo.alt}
          />
        </div>
      ))}
    </div>
  );
};

export default Customers;
