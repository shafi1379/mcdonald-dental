import Image from "next/image";
import { ImageDetail, TextDescription } from "../utilities/util";

type Props = {
  details: {
    feature_illustration: ImageDetail;
    title: TextDescription[];
    description: TextDescription[];
    button_lebel: string;
  };
};

const ImgDesc: React.FC<Props> = ({ details }: Props) => {
  debugger;
  return (
    <div className="text-center mx-auto py-10 px-5 md:py-20 md:px-20">
      <div className="grid md:mx-7 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2">
        <div>
          <Image
            src={`/api/imageproxy?url=${encodeURIComponent(
              details.feature_illustration.url
            )}`}
            width={524}
            height={288}
            alt={details.title[0].text}
          />
        </div>
        <div className="md:px-10 md:py-5 text-left">
          <h3 className="text-[21px] text-balck-medium">
            {details.title[0].text}
          </h3>
          <p className="text-[16px] mt-4 text-md text-grey-medium mb-10 max-w-367">
            {details.description[0].text}
          </p>
          <a
            href="#"
            rel="noreferrer"
            className="text-[16px] font-normal text-grey-medium cursor-pointer"
          >
            {details.button_lebel}
            <i
              className="fa fa-long-arrow-right ml-2 text-orange-medium"
              aria-hidden="true"
            ></i>
          </a>
        </div>
      </div>
    </div>
  );
};

export default ImgDesc;
