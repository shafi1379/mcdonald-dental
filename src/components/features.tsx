import { ServiceCatagories, TextDescription } from "../utilities/util";

type Props = {
  categories: {
    primary: {
      title1: TextDescription[];
    };
    items?: ServiceCatagories[];
  };
};

const Features: React.FC<Props> = ({ categories }: Props) => {
  debugger;
  return (
    <div className="text-center mx-auto py-10 px-4 mt-5">
      <h2 className="text-[34px]">
        {categories.primary.title1[0].text}
        <span className="text-[#d25f44]">.</span>
      </h2>
      <div className="py-10 px-5 md:px-20 pb-20 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 gap-5">
        {categories.items.map((category: ServiceCatagories, ind: number) => (
          <div
            key={ind}
            style={{ background: category.bgcolor }}
            className="p-5 m-5 rounded-xl overflow-hidden shadow-lg"
          >
            <div className="px-6 py-4 text-white">
              <div className="text-center pb-5">
                <i
                  className={`far bg-white-off text-blue-dark fa-${category.icon} text-[21px] rounded-full p-2.5`}
                  aria-hidden="true"
                ></i>
              </div>
              <div className="font-normal text-[21px] mb-2">
                {category.header}
              </div>
              <p className="text-[16px]">{category.description}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Features;
