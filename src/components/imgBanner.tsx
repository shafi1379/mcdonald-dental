import Image from "next/image";
import cn from "classnames";
import { ImageDetail, TextDescription } from "../utilities/util";

type Props = {
  banner?: {
    image_banner: ImageDetail;
    image_description: TextDescription[];
  };
};

const ImgBanner: React.FC<Props> = ({ banner }: Props) => {
  return (
    <div className="relative h-[656px]">
      <Image
        className="bg-image"
        layout="fill"
        objectFit="cover"
        src={`/api/imageproxy?url=${encodeURIComponent(
          banner.image_banner.url
        )}`}
        alt={banner.image_description[0].text}
      />
      <div className="bg-cover other-bg-img"></div>
      <div className="content pt-60">
        <div className={cn("text-center mx-auto")}>
          <h1 className="text-[42px] font font-normal text-white">
            {banner.image_description[0].text}
            <span className="border-b-8 w-24 rounded block m-auto mt-5 border-orange-medium"></span>
          </h1>
        </div>
      </div>
    </div>
  );
};

export default ImgBanner;
